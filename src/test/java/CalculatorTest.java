import com.aqa.course.Calculator;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/**
 * CalculatorTest includes tests for Calculator class.
 *
 * @author alexpshe
 * @version 1.0
 */
public class CalculatorTest {
    private Calculator calculator;
    private SoftAssertions softly;

    @BeforeAll
    public static void setupBeforeAllTest() {
    }

    // метод, выполняемый перед каждым тестом
    @BeforeEach
    public void setup() {
        this.calculator = new Calculator();
        this.softly = new SoftAssertions();
    }

    @AfterEach
    public void assertAll() {
        this.softly.assertAll();
    }

    @RepeatedTest(value = 2, name = "{displayName} {currentRepetition}/{totalRepetitions}")
    public void repeatedTest() {
    }

    @ParameterizedTest
    @ValueSource(ints = { 1, -10 })
    public void parameterizedWithValuesTest() {
    }

    @DisplayName("Base positive test for sum")
    @Tag("positive")
    @ParameterizedTest
    @CsvSource({
            "1, 2, 3", // тест кейс 1
            "-10, -20, -30" // тест кейс 2
    })
    public void sumTest(int firstValue, int secondValue, int expectedResult) {
        int actualResult = calculator.sum(firstValue, secondValue);

        softly.assertThat(actualResult).isEqualTo(expectedResult);
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/test-data.csv")
    public void divideTest(int firstValue, int secondValue, int expectedResult) {
        double actualResult = calculator.divide(firstValue, secondValue);

        softly.assertThat(actualResult).isEqualTo(expectedResult);
    }

    private static Stream<Arguments> testDataForSumOfCollectionValues() {
        return Stream.of(
                Arguments.of(Arrays.asList(1, 3, 4), 8), // тест кейс 1
                Arguments.of(Arrays.asList(-1, 0, 1), 0) // тест кейс 2
        );
    }

    @Disabled
    @ParameterizedTest
    @MethodSource("testDataForSumOfCollectionValues")
    public void sumOfCollectionValuesTest(List<Integer> numbers, long expectedResult) {
        long actualResult = calculator.sum(numbers);

        softly.assertThat(actualResult).isEqualTo(expectedResult);
    }

    @Test
    @Disabled
    @Tag("broken")
    public void brokenTest() {
    }

    private static Stream<Arguments> testDataForMultipleOfCollectionValues() {
        return Stream.of(
                Arguments.of(Arrays.asList(4, 3), 12),
                Arguments.of(Arrays.asList(10, 50, 10), 5000)
        );
    }
    @Test
    @DisplayName("For multiplation")
    @Tag("positive")
    @ParameterizedTest
    @MethodSource("testDataForMultipleOfCollectionValues")
    public void multipleTest(List<Integer> numbers1, long expectedResult) {
        long actualResult = calculator.multiple(numbers1);

        softly.assertThat(actualResult).isEqualTo(expectedResult);
    }

    @Test
    @DisplayName("For powation")
    @Tag("positive")
    @ParameterizedTest
    @CsvSource({
                    "2, 3, 8", // тест кейс 1
                    "-10, 1, -10" // тест кейс 2
            })
    public void powTest(int number, int power, int expectedResult){
        long actualResult = calculator.pow(number,power);

        softly.assertThat(actualResult).isEqualTo(expectedResult);
    }


}
